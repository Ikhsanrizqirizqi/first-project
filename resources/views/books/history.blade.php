@extends('layouts.template')

@section('content')

                    <div class="col-md-12 col-sm-12">
                    <div class="row">
                        <div class="x_panel">
                        <div class="x_title">
                        <h2>Data Peminjaman Buku Siswa</h2>
                        <br>
                        <ul class="nav navbar-right panel_toolbox">
                        <li>
                        </li>
                        </ul>
                        
                        <br>
                        </div>
                        <div class="clearfix"></div>
                        <div class="x_content">
                        <div class="col-md-3 col-sm-3 profile_left">
                        <div class="profil_img">
                        <div id="crop-avatar">

                        <img class="img-responsive avatar-view" src="{{asset('img.png')}}" alt="Avatar" style="height: 150px; widht: 160px;">
                        </div>
                        </div>
                        <br>                       
                      <ul class="list-unstyled user_data">
                        <li>
                        <i class="fa fa-user user-profile-icon">Penulis : {{ $book->author }} </i> 
                        </li>

                        <li>
                          <i class="fa fa-user-md user-profile-icon">Penerbit : {{ $book->publisher }} </i> 
                        </li>
                        <li>
                          <i class="fa fa-calendar user-profile-icon">Tahun : {{ $book->year }} </i> 
                        </li>
                      </ul>
                      <br/>
                      </div>
                     
                      <div class="col-md-9 col-sm-9">
                       <div class="table">
                      <table class="data table table-striped no-margin">
                              <thead>
                                <tr style="text-align: center;">
                                  <th>#</th>
                                  <th>Nama</th>
                                  <th>Buku</th>
                                  <th>Peminjam</th>
                                  <th>Kembali</th>
                                  <th>Denda</th>
                                
                                </tr>
                              </thead>
                              </tbody>

                                 @foreach ($book->peminjam as $i => $item)
                                 <tr style="text-align: center;">
                                    <td>{{ $i+1 }}</td>
                                    <td>{{ $item->borosRef->name }}</td>
                                    <td>{{ $item->boroRef->title }}</td>
                                    <td>{{ $item->start }}</td>
                                    <td>{{ $item->return }}</td>
                                    <td>Rp.{{ $item->denda }}</td>
                                  </tr>
                                  @endforeach  
                                   
                              </tbody>
                            </table>                           
                          </div>                        
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
           @endsection