@extends('layouts.template')

@section('content')
<div class="x_panel">
                  <div class="x_title">
                    <h2>Tambah Data Kategori</h2>
                    <ul class="nav navbar-right panel_toolbox">
                     
                     
                      
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                   
                    <form action="{{ route('book_category.store') }}" method="post">
                            @csrf
                                <div class="form-row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="name">Nama</label>
                                            <input type="text" name="name" class="form-control" 
                                            id="name" placeholder="Masukkan Nama">
                                        </div>
                                        <div class="form-group">
                                            <label for="description">Deskripsi</label>
                                            <textarea class="form-control" name="description" 
                                            id="description" rows="4"></textarea>
                                        </div>
                                    </div>
                                </div>
                                <button type="submit" class="btn btn-danger">Simpan</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
