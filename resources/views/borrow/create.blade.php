@extends('layouts.template')

@section('content')
<div class="x_panel">
                  <div class="x_title">
                    <h2>Tambah Data Peminjaman</h2>
                    <ul class="nav navbar-right panel_toolbox">
                     
                     
                      
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                   
                    <form action="{{ route('borrow.store') }}" method="post">
                            @csrf
                                <div class="form-row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="siswa">Siswa</label>
                                            <select class="form-control" id="siswa" name="siswa">
                                        <option>chose...</option>
                                        @foreach ($siswa as $item)
                                       <option value="{{ $item->id }}">{{ $item->name }}</option>
                                       @endforeach
                                        </select>
                                        </div>
                                        </div>
                                        <div class="form-group col-md-6">
                                        <label for="books">Buku</label>
                                        <select class="form-control" id="books" name="books">
                                        <option>chose...</option>
                                        @foreach ($books as $item)
                                       <option value="{{ $item->id }}">{{ $item->title }}</option>
                                       @endforeach
                                        </select>
                                         </div>
                                        
                                        <div class="form-group col-md-6">
                                            <label for="date">Tanggal Pinjam</label>
                                            <input type="date" class="form-control" id="date" 
                                            name="date" placeholder="Masukan Tanggal Pinjam">
                                        </div>
                                    </div>
                                <button type="submit" class="btn btn-danger">Simpan</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
@endsection
